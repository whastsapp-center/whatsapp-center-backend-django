from django.conf.urls import url
from django.urls import path
from . import views

app_name='whatsapp-center'

urlpatterns=[
    # path para acceder al  listado de empresas  /api/empresa/
    url(r'^empresa/$', views.empresa_list),
    # path para acceder a validar las credenciales del usuario /api/login/
    url(r'^login/$', views.login),
    # path para acceder a los contactos asignados a un usuario  /api/asignacion/?
    url(r'^asignacion/(?P<pk>[0-9]+)/$', views.contactos_list),
    # path para acceder a los contactos asignados a un usuario  /api/mensajes/?
    url(r'^mensajes/(?P<pk>[0-9]+)/$', views.mensajes_list)
]
