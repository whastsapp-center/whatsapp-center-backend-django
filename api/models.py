from django.db import models

# MAPEO DE  NUESTRAS TABLAS EN MODELOS PARA REALIZAR CONSULTAS EN NUESTRA BD
class Empresa(models.Model):
    idempresa=models.IntegerField(primary_key=True)
    razonsocial=models.TextField()
    class Meta: db_table = 'empresa'

class Usuario(models.Model):
    idusuario=models.IntegerField(primary_key=True)
    idempresa = models.IntegerField()
    apellidos = models.TextField()
    nombres = models.TextField()
    usuario = models.TextField()
    clave = models.TextField()
    idrol = models.IntegerField()
    class Meta: db_table = 'usuario'

class Asignacion(models.Model):
    idasignacion=models.IntegerField(primary_key=True)
    idusuario = models.IntegerField()
    idcontacto = models.IntegerField()
    class Meta: db_table = 'asignacion'

class Contacto(models.Model):
    idcontacto=models.IntegerField(primary_key=True)
    idnumero = models.IntegerField()
    nombre = models.TextField()
    codigo = models.TextField()
    fconexion = models.DateTimeField()
    class Meta: db_table = 'contacto'

class NumeroTelefono(models.Model):
    idnumero=models.IntegerField(primary_key=True)
    idempresa = models.IntegerField()
    numero = models.TextField()
    instance = models.TextField()
    token = models.TextField()
    class Meta: db_table = 'numero_telefono'

#MODELO DTO PARA DEVOLVER LOS MENSAJES DE CADA CONTACTO
class MessagesDTO(models.Model):
    body = models.TextField()
    fechaHora = models.TextField()
