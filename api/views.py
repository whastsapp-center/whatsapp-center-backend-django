from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from api.models import Empresa, Usuario, Asignacion, Contacto, NumeroTelefono, MessagesDTO
from api.serializers import EmpresaSerializer, UsuarioSerializer, ContactoSerializer, MessagesDTOSerializer
import requests
import json
import datetime
import bcrypt

class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

#FUNCION PARA REALIZAR EL LISTADO DE LAS SEMPRESAS
@csrf_exempt
def empresa_list(request):
    if request.method == 'GET':
        listaEmpresa = Empresa.objects.all()
        #SE RETORNA EL LISTADO EN FORMATO JSON
        serializer = EmpresaSerializer(listaEmpresa, many=True)
        return JSONResponse(serializer.data)

#FUNCION PARA REALIZAR LA VALIDACION DEL LOGIN
@csrf_exempt
def login(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        usuario=Usuario.objects.filter(usuario=data['usuario']).first()

        if(usuario):
            if bcrypt.checkpw(data['clave'].encode("utf-8"), usuario.clave.encode('utf-8')):
                pass
            else:
                usuario=Usuario()

        # SE RETORNA EL OBJETO EN FORMATO JSON
        serializer = UsuarioSerializer(usuario)
        return JSONResponse(serializer.data)

#FUNCION PARA DEVOLVER LOS CONTACTOS ASIGNADOS A UN USUARIO
@csrf_exempt
def contactos_list(request,pk):
    if request.method == 'GET':
        listaAsignacion = Asignacion.objects.filter(idusuario=pk)
        listaContactos=[]

        for asignacion in listaAsignacion:
            contacto=Contacto.objects.get(idcontacto=asignacion.idcontacto)
            listaContactos.append(contacto)

        # SE RETORNA EL LISTADO EN FORMATO JSON
        serializer = ContactoSerializer(listaContactos, many=True)
        return JSONResponse(serializer.data)

#FUNCION PARA DEVOLCER LOS MENSAJES DE UN CONTACTO
@csrf_exempt
def mensajes_list(request,pk):
    if request.method == 'GET':

        contacto = Contacto.objects.get(idcontacto=pk)
        numeroTelefono= NumeroTelefono.objects.get(idnumero=contacto.idnumero)
        # SE REALIZA LA  PETICION DE LOS MENSAJES DE WHATSAPP
        url = "https://api.chat-api.com/instance"+numeroTelefono.instance+ "/messages?token="+numeroTelefono.token+"&chatId="+contacto.codigo ;

        response = requests.get(url=url)
        diccionario=json.loads(response.text)

        listaMensajes=diccionario['messages']
        listaDtoMensaje = []

        for mensaje in listaMensajes:
            dto=MessagesDTO()
            dto.body=mensaje['body'].replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u').replace('?','').replace('¿','').replace('!','').replace('¡','')
            timestamp =mensaje['time']
            value = datetime.datetime.fromtimestamp(timestamp)
            dto.fechaHora = value.strftime('%Y-%m-%d %H:%M')
            if(mensaje['fromMe']==False):
                listaDtoMensaje.append(dto)

        listaDtoMensaje.sort(key=lambda x: x.fechaHora)

        # SE RETORNA EL LISTADO EN FORMATO JSON
        serializer = MessagesDTOSerializer(listaDtoMensaje, many=True)
        return JSONResponse(serializer.data)


