from rest_framework import serializers
from api.models import Empresa, Usuario, Contacto, MessagesDTO

#CREAMOS LA SERIALIZACIÓN,  LOS CAMPOS SE VAN A DEVOLVER PARA CADA PETICION
class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model=Empresa
        fields=('idempresa','razonsocial')

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model=Usuario
        fields=('idusuario','idempresa','apellidos','nombres','idrol')

class ContactoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Contacto
        fields=('idcontacto','idnumero','nombre')

class MessagesDTOSerializer(serializers.ModelSerializer):
    class Meta:
        model=MessagesDTO
        fields=('body','fechaHora')
